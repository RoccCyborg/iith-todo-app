"use client";

import { ChangeEvent, useState } from "react";
import { api } from "~/trpc/react";
import { RouterOutputs } from "~/trpc/shared";

interface TodoProps {
  todos: RouterOutputs["todo"]["getTodos"];
}

function Todos({ todos }: TodoProps) {
  const utils = api.useUtils();
  const [text, setText] = useState("");
  const [selectedTodo, setSelectedTodo] = useState<number>();

  const mutation = api.todo.deleteTodo.useMutation();
  const updateMutation = api.todo.updateTodo.useMutation();

  const handleDelete = async (id: number) => {
    await mutation.mutateAsync({ id: id });
    await utils.todo.getTodos.invalidate();
  };

  const handleToggle = async (e: ChangeEvent<HTMLInputElement>, id: number) => {
    await updateMutation.mutateAsync({
      id: id,
      status: e.target.checked,
    });
    await utils.todo.getTodos.invalidate();
  };

  const handleEdit = async (
    todo: RouterOutputs["todo"]["getTodos"][number],
  ) => {
    if (todo.id === selectedTodo) {
      setSelectedTodo(undefined);
      await updateMutation.mutateAsync({ id: todo.id, text: text });
      await utils.todo.getTodos.invalidate();
    } else {
      setText(todo.text);
      setSelectedTodo(todo.id);
    }
  };

  return (
    <div className="p-2">
      {todos.map((todo) => {
        return (
          <div key={todo.id} className="flex justify-between p-1">
            <div className="flex items-center">
              <input
                autoFocus
                type="checkbox"
                className="mx-2"
                checked={todo.status}
                onChange={(e) => handleToggle(e, todo.id)}
              />
              {selectedTodo === todo.id ? (
                <input
                  type="text"
                  className=" w-full rounded border p-1"
                  value={text}
                  onChange={(e) => setText(e.target.value)}
                />
              ) : (
                todo.text
              )}
            </div>
            <div className="flex gap-3">
              <button
                className="bg-gray-300 p-2"
                onClick={() => handleEdit(todo)}
              >
                {selectedTodo == todo.id ? "Save" : "Edit"}
              </button>
              <button
                className="bg-gray-300 p-2"
                onClick={() => handleDelete(todo.id)}
              >
                Delete
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default function Home() {
  const utils = api.useUtils();
  const [text, setText] = useState("");

  const todosQuery = api.todo.getTodos.useQuery();

  const mutation = api.todo.createTodo.useMutation();

  const handleAdd = async () => {
    console.log("Add TODO");
    console.log(text);
    await mutation.mutateAsync({ text: text });
    setText("");
    await utils.todo.getTodos.invalidate();
  };

  return (
    <main className="flex min-h-screen">
      <div className="container gap-12 px-4 py-16 ">
        <div>
          <h1 className="text-center text-3xl">Todo Application</h1>
        </div>
        <div>
          <div>
            <input
              type="text"
              className="m-2 w-full rounded border p-2"
              value={text}
              onChange={(e) => setText(e.target.value)}
            />
          </div>
        </div>
        <div>
          <button className="rounded bg-gray-200 p-2" onClick={handleAdd}>
            Add Task
          </button>
        </div>
        <div>{todosQuery.data ? <Todos todos={todosQuery.data} /> : null}</div>
      </div>
    </main>
  );
}
